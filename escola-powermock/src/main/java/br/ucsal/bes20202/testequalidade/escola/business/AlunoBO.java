package br.ucsal.bes20202.testequalidade.escola.business;

import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20202.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20202.testequalidade.escola.util.DateUtil;

public class AlunoBO {

	public static void salvar(Aluno aluno) {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			AlunoDAO.salvar(aluno);
		}
	}

	public static Integer calcularIdade(Integer matricula) {
		Aluno aluno = AlunoDAO.encontrarPorMatricula(matricula);
		return DateUtil.obterAnoAtual() - aluno.getAnoNascimento();
	}

}
