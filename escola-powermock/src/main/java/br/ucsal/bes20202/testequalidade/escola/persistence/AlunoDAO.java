package br.ucsal.bes20202.testequalidade.escola.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.domain.SituacaoAluno;

//create table aluno
//(matricula integer not null primary key,
// nome varchar(60) not null,
// situacao varchar(20) not null,
// anoNascimento integer not null);
public class AlunoDAO extends AbstractDAO {

	public static void salvar(Aluno aluno) {
		System.out.println(aluno);
		String sql = "insert into aluno (matricula, nome, situacao, anoNascimento) values (?, ?, ?, ?)";

		try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
			statement.setInt(1, aluno.getMatricula());
			statement.setString(2, aluno.getNome());
			statement.setString(3, aluno.getSituacao().toString());
			statement.setInt(4, aluno.getAnoNascimento());
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void excluirTodos() {
		String sql = "delete from aluno";

		try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Aluno encontrarPorMatricula(Integer matricula) {
		String sql = "select nome, situacao, anoNascimento ";
		sql += " from aluno ";
		sql += " where matricula = " + matricula;

		try (Statement statement = getConnection().createStatement();
				ResultSet resultSet = statement.executeQuery(sql);) {
			if (resultSet.next()) {
				return resultSetToAluno(matricula, resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Aluno resultSetToAluno(Integer matricula, ResultSet resultSet) throws SQLException {
		String nome = resultSet.getString("nome");
		String situacao = resultSet.getString("situacao");
		Integer anoNascimento = resultSet.getInt("anoNascimento");
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setAnoNascimento(anoNascimento);
		aluno.setSituacao(SituacaoAluno.valueOf(situacao));
		return aluno;
	}

}
