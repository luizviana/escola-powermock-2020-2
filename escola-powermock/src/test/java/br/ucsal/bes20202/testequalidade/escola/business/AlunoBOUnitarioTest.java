package br.ucsal.bes20202.testequalidade.escola.business;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20202.testequalidade.escola.builders.AlunoBuilder;
import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.persistence.AbstractDAO;
import br.ucsal.bes20202.testequalidade.escola.persistence.AlunoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AlunoBO.class, AlunoDAO.class, AbstractDAO.class })
public class AlunoBOUnitarioTest {

	/**
	 * 
	 * Caso de teste 1: Verificar se alunos ativos são salvos.
	 * 
	 * Entrada: uma instância de aluno ativo;
	 * 
	 * Saída esperada: como este método é void, para avaliação seu
	 * comportamento você deve verificar se houve chamada ao método salvar da
	 * classe AlunoDAO.
	 *
	 * Obs1: lembre-se de que o teste é unitário, ou seja, você deve mocar a
	 * classe AlunoDAO.
	 * 
	 * Obs2: NÃO é necessário mocar o método getSituacao().
	 * 
	 * @throws Exception
	 * @throws SQLException
	 *
	 */

	@Test
	public void testarSalvamentoAlunoAtivo() throws SQLException, Exception {

		Aluno aluno = AlunoBuilder.umAluno().comMatricula(25).comAnoNascimento(2002).build();


		AlunoDAO alunoDAO = PowerMockito.mock(AlunoDAO.class);
		PowerMockito.doNothing().when(alunoDAO).salvar(aluno);

		AlunoBO.salvar(aluno);
		
		PowerMockito.verifyStatic(AlunoDAO.class);
		AlunoDAO.salvar(aluno);

	}

}
