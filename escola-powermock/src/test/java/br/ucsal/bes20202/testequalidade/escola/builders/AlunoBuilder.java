package br.ucsal.bes20202.testequalidade.escola.builders;

import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	
	
	private static final Integer MATRICULA_DEFAULT = null;
	private static final String NOME_DEFAULT = "Brian Schultz";
	private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;
	private static final Integer ANO_DEFAULT = null;
	
	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_DEFAULT;

	private AlunoBuilder() {

	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}
	
	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}
	
	public AlunoBuilder comSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	
	
	public AlunoBuilder mas() {
        return umAluno().comMatricula(matricula).comNome(nome).comSituacao(situacao).comAnoNascimento(anoNascimento);
    }
	
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
